import subprocess
import re
from os import path


def get_db_settings(filestr, patterns):

    # Let's get a new dictionary ready to hold the results of
    # our searches.

    settings = {}

    # Loop through each supplied pattern, adding matches to the
    # results dictionary (settings variable). It is very important
    # that patterns use named groups here. If groups are not named,
    # we will get no results.

    for name, pattern in patterns.items():
        match = re.search(pattern, filestr)
        if match:
            for key, value in match.groupdict().items():
                settings[key] = value

    # At this point, we have either an empty dictionary, or one with
    # some results. However, if we don't have all 4 of the values
    # we're looking for, it doesn't matter. Let's do some testing.

    test = True

    test = test and "host" in settings
    test = test and "db" in settings
    test = test and "user" in settings
    test = test and "pw" in settings

    # So now assuming we've found all of the results we needed, we
    # should have a value of True in test. One more quick check.
    # We're only supporting MySQL here, so other databases will
    # not be backed up.

    if "driver" in settings and settings["driver"] != "mysql":
        test = False

    if test:
        return settings

    return None


def create_db_backup(config, temp_file):

    # Run mysqldump with provided settings. If the file was created
    # successfully, then return True. Otherwise return False.

    args = [
        "mysqldump",
        "-h", config["host"],
        "-u", config["user"],
        "-p" + config["pw"],
        config["db"], ">", temp_file
    ]

    try:
        subprocess.check_call(args, shell=True)
    except subprocess.CalledProcessError:
        return False

    if not path.exists(temp_file):
        return False

    return True






