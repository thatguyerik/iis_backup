import re
from os import path

import db


def check_wordpress(site_path):

    # The wp-config.php file is constant across all versions of Wordpress. If
    # it does not exist, we can safely assume this is not a Wordpress site.

    if path.exists(site_path + "\\wp-config.php"):
        return True

    return False


def get_wordpress_db(site_path):

    patterns = {
        "host": re.compile("^\s*define\('DB_HOST',\s+'(?P<host>.+)'\);.*$", re.MULTILINE | re.IGNORECASE),
        "db": re.compile("^\s*define\('DB_NAME',\s+'(?P<db>.+)'\);.*$", re.MULTILINE | re.IGNORECASE),
        "user": re.compile("^\s*define\('DB_USER',\s+'(?P<user>.+)'\);.*$", re.MULTILINE | re.IGNORECASE),
        "pw": re.compile("^\s*define\('DB_PASSWORD',\s+'(?P<pw>.+)'\);.*$", re.MULTILINE | re.IGNORECASE)
    }

    try:
        settings_file = open(path.join(site_path, "wp-config.php"), "r")
        settings_string = settings_file.read()
        settings_file.close()
    except FileNotFoundError:
        return None

    settings = db.get_db_settings(settings_string, patterns)

    return settings
