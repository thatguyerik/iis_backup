import os
import subprocess
import xml.etree.ElementTree as eTree
import zipfile
import re
import getopt
import sys
import shutil
import ftplib
from os import path
from datetime import date

import db
import drupal
import drush
import wordpress

backup_dir = None
backup_ftp = None
temp_dir = "C:\\Temp"
verbose = False
subject_site = None


def iis_get_sites():

    # Gets a list of paths to the sites hosted on the machine's IIS web server.
    # There were multiple issues getting the Python interpreter to read the
    # applicationHost.config file, so the workaround was to call AppCmd.exe to
    # list the virtual directories in XML format. This only works on Windows.

    sites = {}

    try:

        # Call AppCmd.exe to output XML containing the paths to each site root.
        # Store the physical path in the sites[] list to be used later.

        sites_xml = subprocess.check_output(["appcmd", "list", "vdir", "/xml"])
        tree = eTree.fromstring(sites_xml)
        for leaf in tree:
            sites[sanitize_site_name(leaf.attrib["APP.NAME"])] = leaf.attrib["physicalPath"]

    except subprocess.CalledProcessError:

        # If the call to AppCmd.exe fails, then we really can't do anything else.
        # Let's exit with some dignity, shall we?

        print("Could not execute AppCmd.exe. Please make sure its folder is in your PATH.")
        sys.exit(1)

    return sites


def sanitize_site_name(name):

    # For some reasons, site names come out of AppCmd with a trailing slash (/). We need
    # to remove that, and replace all spaces with underscores so we can use this as a
    # filename later. We'll also return it in all lowercase.

    blanks_pattern = re.compile("\s+")
    slash_pattern = re.compile("[/()]+")

    sanitized_name = name
    sanitized_name = re.sub(blanks_pattern, "_", sanitized_name)
    sanitized_name = re.sub(slash_pattern, "", sanitized_name)

    return sanitized_name.lower()


def archive_site(site_path, db_path, zip_path):

    global verbose
    global temp_dir

    # Compress the site's files down to a single archive for storage.

    zf = zipfile.ZipFile(zip_path, "w")

    # If there is a database file to include, add it to the archive.
    if path.exists(db_path):
        zf.write(db_path, path.join("database", path.relpath(db_path, temp_dir)))

    # Add all files from the site root.
    for root, dirs, files in os.walk(site_path):
        for file in files:
            try:
                zf.write(path.join(root, file), path.join("website", path.relpath(path.join(root, file), site_path)))
            except FileNotFoundError:
                # In Windows, this usually means a permission error on a file.
                # If this happens, let's just ignore it and move on.
                pass
            except OSError as ose:
                # This could be for any number of reasons, the most common of
                # which is that the file system is out of space. We need to
                # get out of this loop and close the zip file.
                print(str(ose))
                zf.close()
                if verbose:
                    print("Stopping backup due to OS error.")
                return False

    zf.close()

    if path.exists(zip_path):
        return True

    return False


def config_ftp(connection_string):

    global backup_ftp

    # We'll need to parse the FTP connection string supplied via the
    # command line. This should be in the format:
    #
    # username:password@ftp.example.com
    #
    # This function should not reassign the global backup_ftp variable
    # in the event that this pattern is not matched.

    cstr = re.compile("^(?P<user>[^:]+):(?P<pass>[^@]+)@(?P<host>[^/]+)(?P<dir>.*)$", re.IGNORECASE)
    m = cstr.match(connection_string)

    if m:

        user = m.group("user")
        pwd = m.group("pass")
        host = m.group("host")

        if verbose:
            print("Opening FTP connection on %s for user %s." % (host, user))

        try:

            backup_ftp = ftplib.FTP(host, user, pwd)

            if m.group("dir"):
                if verbose:
                    print("Changing FTP working directory to %s" % m.group("dir"))
                backup_ftp.cwd(m.group("dir"))
            else:
                backup_ftp.cwd("/")

        except ftplib.all_errors:

            print("Could not connect to %s" % host)

    else:

        print("Your FTP connection string appears to be invalid. Executing local backup only.")


def parse_args():

    global subject_site
    global backup_dir
    global backup_ftp
    global verbose

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hd:f:vs:", ["help", "backup-dir", "ftp", "verbose", "site"])
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    for o, a in opts:
        if o in ("-d", "--backup-dir"):
            backup_dir = a
        elif o in ("-h", "--help"):
            display_help()
            sys.exit(0)
        elif o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-s", "--site"):
            subject_site = a
        elif o in ("-f", "--ftp"):
            config_ftp(a)
        else:
            print("Unrecognized argument \"" + o + "\"")
            sys.exit(2)

    if not backup_dir and not backup_ftp:
        print("You must specify either a local directory or FTP connection.")
        sys.exit(2)


def display_help():

    # TODO: Add help.

    print("Sorry, you're on your own here for now.")


def main():

    global verbose
    global subject_site
    global backup_dir
    global backup_ftp
    global temp_dir
    relative_path = ""
    stamp = date.today()
    stamp = stamp.strftime("%Y-%m-%d")

    # It's possible the user did not specify a backup directory, opting for FTP
    # backup only. In this case, we can make the backup directory the same as the
    # temp directory. This will be used later to skip the file copy operation
    # on the local file system.

    if not backup_dir:
        backup_dir = temp_dir

    # It's possible the user passed in a relative path on the command line. If so,
    # we need to join the user-provided path to the current site's path. This will
    # cause the backup path to be updated with each site.

    backup_path_is_relative = not path.isabs(backup_dir)
    if backup_path_is_relative:
        relative_path = backup_dir

    # Get a list of all sites hosted in IIS.
    sites = iis_get_sites()

    # If looking for a specific site, we should only backup that one site.
    if subject_site is not None:

        # If the specified site is not found, we should stop execution and display an
        # error message.
        if subject_site not in sites:
            print("Site %s does not exist. Exiting." % subject_site)
            sys.exit(2)

        # Otherwise, we need to reduce sites to only the one we want to backup.
        else:
            sites = {subject_site: sites[subject_site]}

    # Loop through sites and execute backup.
    for name, site in sites.items():

        # If we have a relative path, we need to join it to the site path. We will update
        # the global variable so that all subroutines called during archiving are using the
        # same value.
        if backup_path_is_relative:
            backup_dir = path.join(site, relative_path)

        # Make sure the path exists.
        if not path.ismount(backup_dir) and not path.exists(backup_dir):
            try:
                os.mkdir(backup_dir)
            except FileNotFoundError:
                if backup_ftp:
                    # If the backup directory cannot be created, but we have an FTP
                    # connection to use, continue with the backup using FTP method only.
                    print("Unable to create local backup directory. Backup will still be uploaded to FTP.")
                    backup_dir = temp_dir
                else:
                    # If the backup directory cannot be created and we have no FTP connection,
                    # then we cannot do anything. There should not be anything to clean up.
                    # Abort script.
                    print("Unable to create local backup directory. No FTP connection specified. Aborting.")
                    sys.exit(2)

        is_drupal = drupal.check_drupal(site)
        is_wordpress = wordpress.check_wordpress(site)

        zip_path = path.join(temp_dir, name + "_" + stamp + ".zip")
        db_path = path.join(temp_dir, name + "_" + stamp + ".sql")

        if is_drupal:

            if verbose:
                print("Archiving Drupal site %s to %s" % (name, backup_dir))
            db_config = drupal.get_drupal_db(site)

            # For Drupal installations, let's clear the cache
            if drush.clear_cache(site) and verbose:
                print("Cache cleared via Drush")

        elif is_wordpress:

            if verbose:
                print("Archiving WordPress site %s to %s" % (name, backup_dir))
            db_config = wordpress.get_wordpress_db(site)

        else:

            db_config = None

        if db_config:
            if db.create_db_backup(db_config, db_path):
                if archive_site(site, db_path, zip_path):

                    if verbose:
                        print("Site archived successfully.")

                    try:

                        # If the user specified a local backup directory, then copy the
                        # zip archive to that directory. If this is the same as the temp
                        # directory, there is no need to copy.
                        if backup_dir != temp_dir:
                            if verbose:
                                print("Copying %s to %s" % (zip_path, backup_dir))
                            shutil.copy(zip_path, backup_dir)
                            if verbose:
                                print("Finished.")

                        # If a valid FTP connection was specified, upload the zip file.
                        if backup_ftp:
                            if verbose:
                                print("Uploading %s to FTP" % zip_path)
                            zip_handle = open(zip_path, "rb")
                            backup_ftp.storbinary("STOR " + name + "_" + stamp + ".zip", zip_handle)
                            zip_handle.close()
                            if verbose:
                                print("Finished.")

                        # When finished, delete the zip file. It's been copied to the specified
                        # backup directory and/or FTP location, so it is no longer required.
                        if verbose:
                            print("Cleaning up. Deleting %s" % zip_path)
                        os.remove(zip_path)

                    except shutil.SameFileError:

                        # UPDATED: This code should never be reached since we are now
                        # testing temp and backup directories for equality prior to
                        # attempting the copy operation.

                        # Temp and Backup paths are the same. No need to move.
                        if verbose:
                            print("Temp and backup location are the same. No need to copy.")

                    finally:

                        # Always remove the database backup, since it exists in the archive
                        # already.

                        if verbose:
                            print("Cleaning up. Deleting %s" % db_path)

                        os.remove(db_path)

            else:
                if verbose:
                    print("Could not create database backup.")
        else:
            if (is_drupal or is_wordpress) and verbose:
                print("Could not parse config file for database settings.")

    # Finally, if we opened an FTP connection, we need to close it.
    if verbose:
        print("Closing FTP connection")
    if backup_ftp:
        backup_ftp.quit()


# This is where the magic begins.
parse_args()
main()
