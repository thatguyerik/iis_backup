import re
from os import path

import db


def check_drupal(site_path):

    # Check for the settings.php file. This location should be the same for
    # Drupal 6 and 7. If this file doesn't exist, we can safely assume this is
    # not a Drupal installation.

    if path.exists(site_path + "\\sites\\default\\settings.php"):
        return True

    return False


def get_drupal_db(site_path):

    # Open settings file for reading database info. This is needed to execute a
    # mysqldump command to get an export of the site's current database. The
    # path of the saved data dump will be sent back to the archive routine for
    # inclusion in the backup archive.

    patterns = {
        "host": re.compile("^\s*'host'\s+=>\s+'(?P<host>.+)',\s*$", re.MULTILINE | re.IGNORECASE),
        "db": re.compile("^\s*'database'\s+=>\s+'(?P<db>.+)',\s*$", re.MULTILINE | re.IGNORECASE),
        "user": re.compile("^\s*'username'\s+=>\s+'(?P<user>.+)',\s*$", re.MULTILINE | re.IGNORECASE),
        "pass": re.compile("^\s*'password'\s+=>\s+'(?P<pw>.+)',\s*$", re.MULTILINE | re.IGNORECASE),
        "driver": re.compile("^\s*'driver'\s+=>\s+'(?P<driver>.+)',\s*$", re.MULTILINE | re.IGNORECASE),
        "v6": re.compile("^\s*\$db_url = 'mysqli://(?P<user>.+):(?P<pw>.+)@(?P<host>.+)/(?P<db>.+)';\s*$",
                         re.MULTILINE | re.IGNORECASE)
    }

    try:

        settings_file = open(path.join(site_path, "sites\\default\\settings.php"), "r")
        settings_string = settings_file.read()
        settings_file.close()

    except FileNotFoundError:

        return None

    # At this point we should have the content of our settings file, and a dictionary full of
    # search patterns to try out. Let's see if we can get anything.

    settings = db.get_db_settings(settings_string, patterns)

    return settings
