import os
import subprocess


def clear_cache(site_path):

    # Clearing the cache can significantly reduce the size of the backup.
    # Plus, this ensures we're not backing up anything we don't really need.

    os.chdir(site_path)

    args = ["drush", "cc", "all"]

    try:
        subprocess.check_call(args, shell=True)
    except subprocess.CalledProcessError:
        # Drush called, but call failed.
        return False
    except FileNotFoundError:
        # Drush not installed on this system
        return False

    return True
