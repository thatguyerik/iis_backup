# IIS Backup

This script is for backing up websites hosted on Windows servers using IIS.

## Requirements

* Python 3.x. This does not have to be in your system path, but it
simplifies things significantly if it's in there.

* The folder containing AppCmd.exe must be in your system path. This
is usually the `C:\Windows\System32\inetsrv` folder.

* The `mysqldump` utility must be installed and in your system path.

## Usage

To use this script, run with `-d` or `--backup-dir` to save a backup
to the local file system. Or, run with `-f` or `--ftp` to upload
the backup to a remote file system. At least one of these parameters
must be present in order for the script to run. Optionally, both may
be used at the same time to create both a local and a remote backup.

The `-d` or `--backup-dir` parameter must be followed by a valid
file system path. If the path provided does not exist, then it will
be created. If it cannot be created due to user permissions then the
script will abort unless an FTP connection was also specified. In that
case, the script will only upload to FTP without making a local backup.

The `-f` or `--ftp` parameter must be followed by an FTP connection
string in the format *username:password@host[/[directory]]*. If
a connection cannot be made a local backup may still proceed if a
backup directory was provided, otherwise the backup will be aborted.

The `-v` or `--verbose` optional parameter can be used to output
progress notifications during archiving.

## Examples

To back up all *Drupal* and *Wordpress* sites to local directory **C:\Backups**:

```
python iis_backup.py -d "C:\Backups"
```

To back up only the site named **ABC Test Site** to local directory **C:\Backups**:

```
python iis_backup.py -s "abc_test_site" -d "C:\Backups"
```

To back up all Drupal and Wordpress sites to FTP host **ftp.example.com**:

```
python iis_backup.py -f "joe_user:secret1234@ftp.example.com"
```

Or for the **/backup** folder of the same FTP site:

```
python iis_backup.py -f "joe_user:secret1234@ftp.example.com/backup"
```

To back up site **ABC Test Site** to both **C:\Backups** and **ftp.example.com/backup** with verbose output:

```
python iis_backup.py -s "abc_test_site" -d "C:\Backups" -f "joe_user:secret1234@ftp.example.com/backup" -v
```

## Important Things to Note

* The script in it's current form only backs up sites detected as
*Drupal* or *Wordpress* installations. Support for other services will
be added at a later date.

* This script is very raw, and has only been tested by me, in
a controlled environment. I would strongly recommend testing it
thoroughly in your own environment before doing something crazy
like scheduling it to run nightly on your production server.
Take this script as a starting point and adapt it to your needs.

## Contributor's Notes

This script leverages AppCmd.exe to pull the list of currently
hosted sites in IIS, and then loops through each one individually,
detecting which content management system, if any, is installed.
It then performs a full recursive backup of the site's root
folder, as well as a database backup file created by leveraging
mysqldump. The backups are archived in one ZIP file per site
and saved to a local directory and/or FTP site passed on the
command line.